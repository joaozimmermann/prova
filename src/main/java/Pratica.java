
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ p1 = new PontoXZ(-3, 2);
        PontoXY p2 = new PontoXY(0,2);
        PontoXZ p3 = new PontoXZ(-3, 2);
        System.out.println("Distancia =  " + p1.dist(p2));
    }
    
}
