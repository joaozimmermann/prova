/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author Joao
 */
public class PontoYZ extends Ponto2D{

    public PontoYZ() {
        
    }

    public PontoYZ(double y, double z) {
        super(0d, y, z);
    }
    
    @Override
    public String toString() {
        return getNome() + "(" + getY() + "," + getZ() + ")";
    }
}
